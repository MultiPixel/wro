#include <Wire.h> //для работы компаса
#include <Servo.h>
#include <NewPing.h> //Сонар

//направления движения робота + стоп
enum movingDirection {STOP = -1, FORWARD, LEFT, BACK, RIGHT};

const int8_t NORTH = 70; //север

const int TIME_TO_DRIVE_30CM = 1700; //задержка для моторов

Servo motorLeft; //mot1 левый
Servo motorReght; //mot2 правый
Servo Hand; //захват

NewPing sonar(4, 5, 40); //подключение сонара,
//__________ (pins, допуск на максимальную дистанцию)

int8_t field[10][6]; //основной массив с полем; 8 на 4 поле + рамка вокруг, чтобы не было выхода за пределы массива

int8_t path[32][2];// массив с сохранённым путём по полю

int8_t numOfSteps=0; //количество пройденных шогов

int8_t globalDirect; //направление движения, из клетки

// структура для кординат
struct Point
{
	int8_t x;
	int8_t y;
	bool operator==(const Point& a) const
	{
		return (x == a.x && y == a.y);
	}
} zeroPoint,tempPoint, globalPosition, globalPositionStart, globalPositionDelta, collectingАreaPoint;
//нулевая,  временная, глобальная (позиция | позиция старта), в случае сбоя,     зона сбора

Point bank[3]; // 3 кординаты баннки

//вспомогательные переменные и массивы для вычислений begin
int8_t dx[4];
int8_t dy[4];
int8_t numStatPoint = 2;
bool randomEachStep; //рандомный поворот направо или на лево, обновляеться каждый шаг (step)
int8_t yTurnFromStep = 0;//костыль
int8_t helparr[10][6];//вспомогательный массив для волнового алгоритма, хранит номар шага
//end

//переменные сенсоров
struct Sensors
{
	int8_t s1, s2, s3, s4, s5, s6, s7, s8, s9;// 0 - черный 1 - белый

	//функция обновляет показания с датчиков
	void update(){}
}sensors;

int8_t getCompas(){} // возвращает значение компаса

struct Motor {
	void motion(int8_t direction){}//элементарные симетричные движения
	void backMoveOverLine(){}
	void TurnOverLine(int8_t direction){}//подготовка к выезду из клетки (если назад то на 180)
	void motorLeft(int8_t direction){}
	void motorRight(int8_t direction){}
}motor;


/** функция, основная задача которой сравнивать поле с пройденым путём (path),
	* возвращает 1 если при старте из i j путь валиден
	*/

int8_t findAround(int8_t i,int8_t j){}// i, j = кординаты

int8_t find() {}//поиск (позиционирование) себя; возвращает кол-во возможных точчек старта

void move(unsigned long dell, int8_t direction = FORWARD){}//двигаеться заданное кол-во времени

void handOpen(){}

void handClose(){}

void turOnLine(){} //встаёт паролельно линии (на линию)

void waveAlgorithm(Point start){} //заполняет вспомогательный массив

void moveTo(Point end, Point start = GlobalPosition){}

void compileBank(){}//собирает банки в оптимальном порядке

void setup(){}

int8_t pow2(int8_t exp){}// 2^exp

//выбирает в какую сторону повернуть
int8_t сhooseDerection(int8_t direction){} // на основе randomEachStep

int8_t flagOfadd16 = 0;//тоже костыль но без него никак; надо ли добавлять БИТ наличия банки

void writeCellInPath(int8_t direction,int8_t compas,int8_t NewDerect){}

//делает поворот, на перекрёстке и
//  записывает это в путь (path)
void turnChoose(int8_t direction){}

void bankEat(){}//поедает банки

int8_t checkField(){}//делает вывод о том, какая ячейка под роботом

void getOut(){}//уезд от банок задом, когда они в зоне склада

Point step(){}//едет из одной клетки в другую, x(.first) содержит клетку, или 32 если нужны дальнейшие исследования; в y (second) лежит данные о том как мы получили эту информацию,(это очень не ясный момент это надо перепродумать)
//Point используеться как std::pair

void rotorBefoOutAndHandOpen(){}//выбирает, в какую сторону удобнее отьезжать(при выгрузке банок), и крутиться, чтобы отьехать удачно 

void mainProgram()//основной порядок девствий; упрощённо:
{
	do
	{
		tmp = step();
		turnChoose(tmp.x);
	}while(numberStatPoint > 1);

	if (numberStatPoint == 0){}// мы ошиблись и потерялись; тут предпринимаем меры
	compileBank();//собрали все банки
	moveTo(collectingАreaPoint);

	//танцы с бубном , чтобы выехать нормально

	rotorBefoOutAndHandOpen();
	getOut();

	MoveTo(GlobalPositionStart);
}