#include <iostream>

using namespace std;

struct coord
{
	int8_t x;
	int8_t y;
	bool operator==(const coord& a) const
	{
		return (x == a.x && y == a.y);
	}
}temp_cord, GlobalPosition, GlobalPositionStart;

coord banka[3];

//global
int8_t NumOfSteps;
int8_t dx[4];
int8_t dy[4];
int8_t path[32][2]; //путь который мы проделали до определения местоположения
int8_t pole[10][6] = //основной массив с полем
{
{-1, -1, -1, -1, -1, -1, },
{-1, 9, 11, 11, 3, -1, },
{-1, 5, 5, 5, 5, -1, },
{-1, 13, 7, 12, 7, -1, },
{-1, 13, 15, 10, 7, -1, },
{-1, 5, 5, 9, 6, -1, },
{-1, 12, 15, 15, 3, -1, },
{-1, 9, 14, 15, 7, -1, },
{-1, 12, 10, 14, 6, -1, },
{-1, -1, -1, -1, -1, -1, },
};
/*
{//от такой записи отказались, хотя она отличает блок 5 от блока 2
	{-1, -1, -1, -1, -1, -1},
	{-1, 2, 3, 3, 2, -1},
	{-1, 1, 1, 1, 1, -1},
	{-1, 3, 3, 2, 3, -1},
	{-1, 3, 4, 1, 3, -1},
	{-1, 1, 1, 2, 2, -1},
	{-1, 2, 4, 4, 2, -1},
	{-1, 2, 3, 4, 3, -1},
	{-1, 2, 1, 3, 2, -1},
	{-1, -1, -1, -1, -1, -1} };
*/
//end global

/** рекурсивная функция, основная задача которой сравнивать поле с путём (path),
	* возвращает 1 если при старте из i j путь валиден
	*	TODO возможно надо добавить припуск прямых
	*/
int8_t findAround(int8_t i,int8_t j)// i, j = кординаты ; k = глубина
{
	for (int k = 0; k < NumOfSteps; ++k)
	{
		if (pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0])
		{
			i += dx[ path[k][1] ];
			j += dy[ path[k][1] ];
		}
		else
		{
			return 0;
		}
	}
	GlobalPosition.x = i;
	GlobalPosition.y = j;
	return 1;
	/*
	if (path[k+1][0] == -1)
	{
		//save x,y
		GlobalPosition.x = i;
		GlobalPosition.y = j;
		return 1;
	}
	else if(pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0])
	{
		return findAround(i + dx[ path[k][1] ],j + dy[ path[k][1] ],k+1);
	}
	else
		return 0;
	*/
}
int8_t Find() {
	int8_t num_p = 0; // переменная хранит количество возможных точек старта

	for (int8_t i = 1; i < 9; i++) {
		for (int8_t j = 1; j < 5; j++) {
			num_p += findAround(i,j);
		}
	}

	GlobalPositionStart = GlobalPosition;
	for (int8_t i = NumOfSteps; i > 0; i--) {
		//pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0]
		GlobalPositionStart.x -= dx[ path[i-1][1] ];
		GlobalPositionStart.y -= dy[ path[i-1][1] ];
	}
	return num_p;
}


int main(int argc, char const *argv[])
{
	//global

	//смешение по x y через направление с.м. README.md
	dx[0]=1; dy[0]=0;
	dx[1]=0; dy[1]=-1;
	dx[2]=-1; dy[2]=0;
	dx[3]=0; dy[3]=1;

	GlobalPosition.x = -1;
	GlobalPosition.y = -1;
	GlobalPositionStart.x = -1;
	GlobalPositionStart.y = -1;
	//global end

	//dev begin --- такие блоки кода исключительно для отладки
	for (size_t i = 0; i < 32; i++) {
		path[i][0] = -1;
		path[i][1] = 0;
	}
	path[0][0] = 0;
	path[0][1] = 1;

	path[1][0] = 13;
	path[1][1] = 0;

	path[2][0] = 13;
	path[2][1] = 3;

	NumOfSteps = 2;//TODO global num of steps

	banka[0].x = 3;
	banka[0].y = 1;

	banka[1].x = 2;
	banka[1].y = 3;

	banka[2].x = 4;
	banka[2].y = 5;
	//dev end
	cout << (int)Find() << endl;

	cout << (int)GlobalPosition.x << " - " << (int)GlobalPosition.y << endl; //dev
	
	cout << (int)GlobalPositionStart.x << " - " << (int)GlobalPositionStart.y << endl; //dev
	return 0;
}
