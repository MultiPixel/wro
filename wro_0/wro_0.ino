#include <Wire.h>
#include <Servo.h>
#include <NewPing.h>

#define FORWARD 0
#define BACK 2
#define LEFT 1
#define RIGHT 3
#define STOP -1

#define NORTH 0

Servo mot1;
Servo mot2;
Servo leftHand;
Servo rightHand;

NewPing sonar(4, 5, 200);

int8_t pole[10][6] = //основной массив с полем
{
	{-1, -1, -1, -1, -1, -1, },
	{-1, 9, 11, 11, 3, -1, },
	{-1, 5, 5, 5, 5, -1, },
	{-1, 13, 7, 12, 7, -1, },
	{-1, 13, 15, 10, 7, -1, },
	{-1, 5, 5, 9, 6, -1, },
	{-1, 12, 15, 15, 3, -1, },
	{-1, 9, 14, 15, 7, -1, },
	{-1, 12, 10, 14, 6, -1, },
	{-1, -1, -1, -1, -1, -1, },
};

int8_t path[32][2];
int8_t NumOfSteps=0;

int8_t dx[4];
int8_t dy[4];

int8_t GlobalDerect;

struct coord
{
	int8_t x;
	int8_t y;
	bool operator==(const coord& a) const
	{
		return (x == a.x && y == a.y);
	}
}temp_cord, GlobalPosition, GlobalPositionStart;

struct Banka
{
	coord Banka1, Banka2, Banka3;
}banka;

struct Sensors
{
	int8_t s1, s2, s3, s4, s5, s6, s7;// 0 BLACK ;1 wight

	//фонкция обновляет показания с датчикой
	void Update()//TODO возможно проверка значений для точности
	{
		s1 = !digitalRead(A2);
		s2 = digitalRead(8);
		s3 = digitalRead(9);
		s4 = digitalRead(6);
		s5 = digitalRead(7);
		s6 = !digitalRead(A1);
		s7 = !digitalRead(A0);
	}
}sensors;

int8_t getCompas()
{
	Wire.beginTransmission(1); // talk to I2C device ID 1
	Wire.write(0x44); // direction register
	Wire.endTransmission(); // end transmission
	Wire.requestFrom(1, 2); // Request 2 bytes from ID 1
	while (Wire.available() < 2);
	uint16_t lb = Wire.read();
	uint16_t val = Wire.read();
	val <<= 8;
	val |= lb;

	// TODO посмотреть , возможно обход ижёт в другую сторону, сейчас по часовой
	val = (val + 45)% 360; // для удобства сравнения
	if ((val >= NORTH)&&(val <= NORTH + 90))
	{
		return FORWARD;
	}
	if ((val > NORTH + 90)&&(val <= NORTH + 180))
	{
		return RIGHT;
	}
	if ((val > NORTH + 180)&&(val <= NORTH + 270))
	{
		return BACK;
	}
	if ((val > NORTH + 270)&&(val <= NORTH + 360))
	{
		return LEFT;
	}

}

/** рекурсивная функция, основная задача которой сравнивать поле с путём (path),
	* возвращает 1 если при старте из i j путь валиден
	*	TODO возможно надо добавить припуск прямых
	*/
int8_t findAround(int8_t i,int8_t j,int8_t k)// i, j = кординаты ; k = глубина
{
	if (path[k+1][0] == -1)
	{
		//save x,y
		GlobalPosition.x = i;
		GlobalPosition.y = j;
		return 1;
	}
	else if(pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0])
	{
		return findAround(i + dx[ path[k][1] ],j + dy[ path[k][1] ],k+1);
	}
	else
		return 0;
}

int8_t Find() {
	int8_t num_p = 0; // переменная хранит количество возможных точек старта

	for (int8_t i = 1; i < 9; i++) {
		for (int8_t j = 1; j < 5; j++) {
			num_p += findAround(i,j,0);
		}
	}

	GlobalPositionStart = GlobalPosition;
	for (int8_t i = NumOfSteps; i > 0; i--) {
		//pole[i + dx[ path[k][1] ]][j + dy[ path[k][1] ]] == path[k+1][0]
		GlobalPositionStart.x -= dx[ path[i-1][1] ];
		GlobalPositionStart.y -= dy[ path[i-1][1] ];
	}
	return num_p;
}

void Rotor(int8_t & sens) {//TODO
	do
	{
		sensors.Update();
	} while (sens == 1);
}

struct Motor {

	int TimeToRotate = 1000;

	void Mot(int8_t der)
	{
		// STOP FORWARD , по линии ? ,
		//TODO servo
		if (der == STOP) {
			M1(0);
			M2(0);
		}
		if (der == FORWARD) {
			M1(100);
			M2(100);
		}
	}

	void Turn90DegrThithLine(int8_t der)
	{
		//TODO servo
		// при FORWARD можно не ехать или чуть прямо
		TimeToRotate = -millis();
		Mot(STOP);
		switch (der) {
			case FORWARD:
			{
				//типо промо TODO
				break;
			}
			case LEFT:
			{

				M2(100);
				delay(500);// FIXME не надёжно
				//XXX можно вынести в вункцию и сделать чтобы при переходе с 0 в 1 остановливавалась
				Rotor(sensors.s7);
				Rotor(sensors.s4);
				/*
				sensors.Update();
				while (sensors.s7 == 1)
				{
					sensors.Update();
				}
				while (sensors.s4 == 1)
				{
					sensors.Update();
				}
				*/
				break;
			}
			case RIGHT:
			{
				M1(100);
				delay(300);// FIXME не надёжно
				Rotor(sensors.s7);
				Rotor(sensors.s3);
				/*
				sensors.Update();
				while (sensors.s7 == 1)
				{
					sensors.Update();
				}
				while (sensors.s3 == 1)
				{
					sensors.Update();
				}
				*/
				break;
			}
		}
		Mot(STOP);
		TimeToRotate += millis();
	}

	void DisTurn90DegrThithLine(int8_t der)
	{
		Mot(STOP);
		switch (der) {
			case FORWARD:
			{
				// TODO
				break;
			}
			case LEFT:
			{
				M2(-100);

				delay(TimeToRotate - 100);//потенциально опасная ситуация: <0 ; можно проехать; можно недоехать
				Rotor(sensors.s6);

				/*
				sensors.Update();
				while (sensors.s6 == 1)
				{
					sensors.Update();
				}
				*/
				break;
			}
			case RIGHT:
			{
				M1(-100);

				delay(TimeToRotate - 100);
				Rotor(sensors.s1);
				/*
				sensors.Update();
				while (sensors.s1 == 1)
				{
					sensors.Update();
				}
				*/
				break;
			}
		}
		Mot(STOP);
	}

	void Turn180()
	{
		//TODO servo если будет плохо работать , можно использовать компас
		//важно не сьехать с линии
		M1(-100);
		M2(100);

		delay(500);

		Rotor(sensors.s1);

		Rotor(sensors.s7);

		Mot(STOP);
	}


	void M1(int8_t der)
	{
		mot1.write(map(der, -100, 100, 0, 180));//не очень точно, map довольно жирный
	}

	void M2(int8_t der)
	{
		mot2.write(map(der, 100, -100, 0, 180));
	}
}motor;


void handOpen()
{
	rightHand.write(80);
	leftHand.write(130);
}
void handClose()
{
	rightHand.write(130);
	leftHand.write(80);
}

void UpToLine()//излишне
{
	M1(-100);
	M2(100);
	Rotor(sensors.s7);
	Mot(STOP);
}


void setup()
{
	Serial.begin(9600);

	Wire.begin();// start I2C
	randomSeed(analogRead(0));

	mot1.attach(10);
	mot2.attach(11);
	rightHand.attach(2);
	leftHand.attach(3);

	for (int i = 6; i <= 9; ++i)
	{
		pinMode(i, INPUT);
	}

	dx[0]=1; dy[0]=0;
	dx[1]=0; dy[1]=-1;
	dx[2]=-1; dy[2]=0;
	dx[3]=0; dy[3]=1;

	GlobalPosition.x = -1;
	GlobalPosition.y = -1;
	GlobalPositionStart.x = -1;
	GlobalPositionStart.y = -1;

	for (size_t i = 0; i < 32; i++) {
		path[i][0] = -1;
		path[i][1] = 0;
	}
}

//выбирает в какую сторону повернуть
int8_t ChooseDerection(int derrect)
{
	/* приоритет : 1)прямо 2) лево 3) прмо
	* нужно лево и право зарандомить иначе можно попасть в кольцо
	*/
	int tmp = 0;
	for (;tmp < 5 ; tmp++)
		if ((derrect / (int)pow(2,tmp) % 2 == 1 )&&(tmp != 2 )&&(tmp < 5))//TODO добавить рандом , что-бы избежать зацикливания
			break;
	return (tmp == 5 ? -1 : tmp);
}

//делает поворот, в на перекрёстке и
//  записывает это в путь (path)
void TurnChoose(int8_t derrect, int8_t oldDerect = -1)
{

	int8_t NewDerect;
	int8_t compas = getCompas();//зачем? есть GlobalDerect

	NewDerect = ChooseDerection(derrect);

	if (NewDerect == -1) {
		motor.Turn180();
	}
	else
	{

		//TurnToDerect(); :

		motor.Turn90DegrThithLine(NewDerect);


		if (sonar.ping_cm() < 45)//провверка на банку; для промого проезда, замерить REVIEW
		{
			motor.DisTurn90DegrThithLine(NewDerect);

			TurnChoose(derrect - pow(2,NewDerect), (oldDerect == -1 ? derrect : oldDerect ) );//TODO проверить чтобы небыло : рекурсия ; можно переделать на вызов ChooseDerection
		}
		else
		{
			derrect = (oldDerect == -1 ? derrect : oldDerect );
			path[NumOfSteps][0] = (derrect == 15 ? 15 : (derrect * (int)pow(2,compas))%15 );// преобразует клетку поля
			path[NumOfSteps][1] = (NewDerect + compas) % 4;//конструкция преобразует поворот из относительных роботу в глобальные кординаты
			//TODO нельзя перезаписывать ; проверить
			NumOfSteps++;

			GlobalDerect = getCompas();
			//едем дальше
		}
	}
}

void loop()
{

	sensors.Update();
	motor.Mot(FORWARD);
	if (!sensors.s3)
		motor.M1(0);
	if (!sensors.s4)
		motor.M2(0);

	int8_t oldNumOfSteps = NumOfSteps;

	if (!sensors.s1 || !sensors.s6 )
	{
		sensors.Update();
		motor.Mot(STOP);
		if (!sensors.s6)
			while(sensors.s5 && sensors.s1)//тут поворачиваемся, для того чтобы исключить ложно-положительный результат: сомнительная функция
			{
				motor.M1(100);//можно меньше
				motor.M2(5);
				sensors.Update();
			}
		motor.Mot(STOP);
		if (!sensors.s1)
			while(sensors.s2 && sensors.s6)
			{
				motor.M2(100);
				motor.M1(5);
				sensors.Update();
			}
		motor.Mot(STOP);

		sensors.Update();
		// XXX подумать над более понимаемым обозначением
		if ((sensors.s7)&&(sensors.s6 != sensors.s1))//2 TODO возможно лучше заминить 6 и 1 на 5 и 2
		{
			TurnChoose((!sensors.s1 ? (2+4) : (8+4)));//2+4 это поворот на лево ; 8+4 поворот на прво (относительно робота) с.м.README
		}else
		if ((!sensors.s7)&&(sensors.s6 != sensors.s1))//3 |-
		{
			TurnChoose((!sensors.s1 ? (2+4+1) : (8+4+1)));
		}else
		if ((sensors.s7)&&(sensors.s6 == 0)&&(sensors.s1 == 0))//3 T
		{
			TurnChoose(4+2+8);
		}else
		if ((!sensors.s7)&&(!sensors.s6)&&(!sensors.s1))//4
		{
			TurnChoose(15);
		}
	}
	if (getCompas() != GlobalDerect)//TODO проверить, при повороте может сработать 2 раз
	{
		TurnChoose(( ((GlobalDerect - getCompas()) ==  1)||((GlobalDerect - getCompas()) ==  -3))? (4+8) : (4+2));
	}

	if (NumOfSteps != oldNumOfSteps)
	{
		find();//if find return 1 then закругляемся и начинаем возить банки
	}
}
